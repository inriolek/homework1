package ua.so.ishchenko.olek;

public class NokiaPhone extends Phone implements PhoneConnection {
    @Override
    void name(){
        System.out.println("Name : Nokia");
    }
    @Override
    void model(){
        System.out.println("Model : 5310");
    }
    @Override
    void memory(){
        System.out.println("Total memory : 512MB");
    }
    @Override
    void ram (){
        System.out.println("RAM : 128MB");
    }
    @Override
    public void sendMessage() {
        System.out.println("Send Message");
    }
    @Override
    public void call() {
        System.out.println("To call");
    }

    }

