package ua.so.ishchenko.olek;

public class Main {
    public static void main(String[] args) {
        SamsungPhone A22 = new SamsungPhone();
        System.out.println("Phone Parameters: ");
        A22.name();
        A22.model();
        A22.memory();
        A22.ram();
        A22.sendMessage();
        A22.call();
        A22.recordVideo();
        A22.takePhoto();
        System.out.println("-----------------");

        NokiaPhone N = new NokiaPhone();
        N.name();
        N.model();
        N.memory();
        N.ram();
        N.call();
        N.sendMessage();


    }
}