package ua.so.ishchenko.olek;

 public class SamsungPhone extends  Phone implements PhoneConnection, PhoneMedia {

    @Override
    void name(){
        System.out.println("Name : Samsung");
    }
    @Override
    void model(){
        System.out.println("Model : A22");
    }
    @Override
    void memory(){
        System.out.println("Total memory : 64 GB");
    }
    @Override
    void ram(){
        System.out.println("RAM : 6GB");
    }

    @Override
    public void sendMessage () {
        System.out.println("Send Message");
    }
    @Override
    public void call () {
        System.out.println("To call");
    }

    @Override
    public void recordVideo() {
        System.out.println("Record Video");
    }
    @Override
    public void takePhoto () {
        System.out.println("Take Photo");
    }
}
