package ua.so.ishchenko.olek;

abstract class Phone {
    abstract void name();

    abstract void model();

    abstract void memory();

    abstract void ram();
}
    interface PhoneConnection
    {
        public void sendMessage();
        public void call();
    }
    interface PhoneMedia
    {
        void takePhoto();
        void recordVideo();
    }
